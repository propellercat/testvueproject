# Test Project:


Eines der beiden folgenden Testaufgaben ist zu bearbeiten. Ein Test ist innerhalb einer Stunde machbar.

## Test V1: Time Logger
Schreibe einen Home-Workout-Time-Logger, wo der User bestimmen kann:

- wie viele Übungen er insgesamt macht (default: 5)
- wie viele Supersets er innerhalb des Zeitfensters macht (default: 3)
- wie lange er eine Teil-Übung macht (default: 30s)
- wie lange er Zeit braucht um zu switchen (Superset-Pause) (default: 5s)
- wie lange er zwischen den Superset-Übungen Zeit hat Pause zu machen (default: 60s)
### Beispiel:

Hier ein Beispiel wie das gemeint ist: https://www.youtube.com/watch?v=T2lyoAhcnXI

```
-Begin (Start)
  -5x Exercise
    -3x Set
      - Zeitanzeige geht von 30s bis 0 (Erste Übung)
      - Zeitanzeige geht von 5s bis 0 (SetBreak)
      - Zeitanzeige geht von 30s bis 0 (Zweite Übung)
      - wenn nicht letzte Set: Zeitanzeige geht von 5s bis 0 (SetBreak)
    - 1x Break
      - wenn nicht letzte Exercise : Pause 60s
- End (Finished)
```

### Code

- optisch anzeigen in welchem Modus sich der User gerade befindet (Start/SetBreak/Exercise/BigBreak/Finished)
- Wert wird auf Funktionalität und Code-Quality gelegt
- optional: "Bing" sound zwischen den Übungs-Pausen-Wechsel
- optional: ansprechendes Design



## Test V2: Fancy Table

Schreibe eine "funktionalle" Tabelle, in der man Reihen filtern und sortieren kann und zusätzlich in Seiten blättern kann

- User darf sortieren
- User darf filtern/suchen
- User kann bestimmen wieviele Einträge pro Seite angezeigt werden
- User kann Seiten ändern

Beispiel json-Datei als Datenbasis ist angelegt

### Code
- Spalten, die sichtbar sein sollen können frei gewählt werden
- Wert wird auf Funktionalität und Code-Quality gelegt


