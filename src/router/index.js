import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/timelogger',
    name: 'Time Logger',
    component: () => import(/* webpackChunkName: "about" */ '../views/TimeLogger.vue')
  },
  {
    path: '/fancytable',
    name: 'Fancy Table',
    component: () => import(/* webpackChunkName: "about" */ '../views/FancyTable.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
